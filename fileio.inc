
procedure StoreEntry(i:byte);
begin
    SetEntry(i);
    dirEntry.attr := fattr;
    dirEntry.size := fsize;
    dirEntry.filetype := FTYPE_UNKNOWN;
    move(@fname,dirEntry.name,Length(fname)+1);
end;

procedure ProcessEntryExtension;
var ext: string[8];
    dot: byte;
begin
    dot:=ord(dirEntry.name[0]);
    ext[0]:=#0;
    repeat 
        Dec(dot);
        Inc(ext[0]);
    until (dirEntry.name[dot]='.') or (ext[0]=#8);
    move(@dirEntry.name[dot+1],@ext[1],byte(ext[0]));
    if (ext='neo') or (ext='NEO') then dirEntry.filetype:=FTYPE_EXECUTABLE;
    if ext='bas' then dirEntry.filetype:=FTYPE_BASICTOKENIZED;
end;

function ReadDir:boolean;
begin
    SetLength(fname,PANEL_WIDTH);
    result := NeoReadDirectory(fname,fsize,fattr);
end;

procedure FetchDir(cp:byte);
var i: word;
    dirname: pointer;
    listCount: byte;
    hidden:boolean;
begin
    dirname := @panel_cwd[cp];
    if not NeoChangeDirectory(dirname) then begin
        panel_cwd[cp]:='/';
        NeoChangeDirectory(dirname);
    end;
    NeoOpenDirectory(dirname);
    dirCount := 0;
    listCount := 0;
    if panel_depth[cp]>0 then
        begin //  has parent
            fname := '..';
            StoreEntry(dirCount);
            dirEntry.attr := 129;
            dirEntry.size := 0;
            entrylist[listCount] := dirCount;
            Inc(dirCount);
            Inc(listCount);
        end;
    
    while ReadDir do  // read and store all not hidden entries 
        begin
            hidden:= (config.showHidden = 0) 
                and (((fattr and FI_ATTR_SYSTEM) <> 0) or 
                    ((fattr and FI_ATTR_HIDDEN) <> 0) or 
                    (fname[1]='.'));
            if not hidden then begin
                StoreEntry(dirCount);
                dirEntry.filetype := FTYPE_NOTINDEXED;
                if isFolder then      // and add folders on top of the entrylist
                    begin
                        dirEntry.filetype := FTYPE_DIR;
                        entrylist[listCount] := dirCount;
                        inc(listCount);
                    end;
                if dirCount=MAX_DIR_ENTRIES-1 then break
                else inc(dirCount);
            end;
        end;

    if config.execFirst = 1 then begin // then add exeutables if option.execFirst
        for i:=0 to dirCount-1 do
            begin
                SetEntry(i);
                if dirEntry.filetype = FTYPE_NOTINDEXED then
                    begin
                        ProcessEntryExtension;
                        if dirEntry.filetype = FTYPE_EXECUTABLE then begin
                            entrylist[listCount] := i;
                            inc(listCount);
                        end else dirEntry.filetype := FTYPE_NOTINDEXED;
                    end;
            end;
    end;

    if dirCount>0 then  // and then all remaining files
        for i:=0 to dirCount-1 do
            begin
                SetEntry(i);
                if dirEntry.filetype = FTYPE_NOTINDEXED then
                    begin
                        ProcessEntryExtension;
                        entrylist[listCount] := i;
                        inc(listCount);
                    end;
            end;
    NeoCloseDirectory;
end;

function DeleteAtPos(cur:byte):boolean;
begin
    SetEntry(entrylist[cur]);
    result := NeoDeleteFile(@dirEntry.name);
end;

function FolderUp:boolean;
var l: byte;
begin
    tmp := panel_cwd[current_panel];
    l := length(tmp);
    while (l>2) do begin    
        dec(l);
        if tmp[l]='/' then break;
    end;
    SetLength(tmp,l-1);
    result := NeoChangeDirectory(@tmp);
    if result then begin
        SetLength(panel_cwd[current_panel],l-1);
        dec(panel_depth[current_panel]);
        panelCursor := panel_curpos[current_panel,panel_depth[current_panel]];
        panelTop := panel_top[current_panel,panel_depth[current_panel]];
        ClearSelected(current_panel);
    end;
end;

function FolderEnter:boolean;
begin
    FullPathToTmp(current_panel,dirEntry.name);
    result := NeoChangeDirectory(@tmp);
    if result then begin
        panel_cwd[current_panel] := tmp;
        panel_curpos[current_panel,panel_depth[current_panel]] := panelCursor;
        panel_top[current_panel,panel_depth[current_panel]] := panelTop;
        inc(panel_depth[current_panel]);
        panelCursor:=0;
        panelTop:=0;
        ClearSelected(current_panel);
    end;
end;

procedure LaunchEntry(i:byte);
var ftype:byte;
begin
    SetEntry(i);
    ftype:=dirEntry.filetype;
    case ftype of
        FTYPE_EXECUTABLE,FTYPE_BASICTOKENIZED: begin
            TextBackground(0);
            TextColor(2);
            ClrScr;
            Writeln('Loading: ',dirEntry.name);
            wordParams[0] := word(@dirEntry.name);
            if ftype = FTYPE_BASICTOKENIZED then asm jmp KERNEL_LOADBAS_ADDRESS; end;
            asm jmp KERNEL_LOADNEO_ADDRESS; end;
        end;
//        FTYPE_BASICTOKENIZED: begin
//            Writeln('Loading: ',dirEntry.name);
//            basicTokenized:=dirEntry.name;
//            asm jmp LOADER_ADDRESS end;
//            readkey;
//        end;
    end; 
end;

function MoveEntry(cur:byte):boolean;
begin
    SetEntry(entrylist[cur]);
    FullPathToTmp(current_panel xor 1,dirEntry.name);
    result := NeoRenameFile(@dirEntry.name,@tmp);
end;

function CopyEntry(cur:byte):boolean;
begin
    SetEntry(entrylist[cur]);
    FullPathToTmp(current_panel xor 1,dirEntry.name);
    result := NeoCopyFile(@dirEntry.name,@tmp);
end;


