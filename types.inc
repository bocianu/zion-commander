type Tfname = string[24];

type TDirEntry =   record
    attr: byte;
    size: cardinal;
    filetype: byte;
    name: Tfname;
end;

type TConfig = record
    showHidden: byte;
    showPanelStatus: byte;
    execFirst: byte;
    saveOnExit: byte;

end;