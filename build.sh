#!/usr/bin/bash
MPPATH=~/Mad-Pascal
MADSPATH=~/Mad-Assembler
NEOPATH=~/neo
NAME='zc'
read ORG < code.org

$MPPATH/mp ${NAME}.pas -target:neo -code:${ORG} && \
$MADSPATH/mads ${NAME}.a65 -x -i:${MPPATH}/base -o:${NAME}.bin && \
python ${NEOPATH}/exec.zip ${NAME}.bin@${ORG} run@${ORG} -o${NAME}.neo && \
$NEOPATH/neo ${NAME}.bin@${ORG} run@${ORG} keys
