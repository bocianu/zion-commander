uses crt,neo6502,neo6502math,neo6502keys;
const
{$i memory.inc}
{$i const.inc}
    VER = '0.61';

{$i types.inc}

var fname: Tfname;
    fsize: cardinal;
    fattr: byte;
    dirEntry: ^TDirEntry;
    entrylist : array [0..MAX_DIR_ENTRIES-1] of byte;
    selected : array [0..1,0..MAX_DIR_ENTRIES-1] of byte;
    dirCount: byte;
    panelCursor: integer;
    panelTop: integer;
    b: byte;
    i: integer;
    joyDelay: byte;
    joyCount: byte;
    quit,reload: boolean;
    panel_x:array [0..1] of byte = (1,PANEL_WIDTH+2);
    panel_cwd: array [0..1] of Tfname;
    current_panel: byte;
    panel_depth: array [0..1] of byte;
    panel_curpos: array [0..1,0..MAX_DEPTH-1] of byte;
    panel_top: array [0..1,0..MAX_DEPTH-1] of byte;
    panel_name: array [0..1] of char = ('L','R');
    tmp: string[90];
    bar: string[90];
    modalTop: byte;
    modalMarginL: byte;
    config: TConfig;
    loadExtName: string[64] absolute $101;
    key:TKey;

    char1: array [0..6] of byte = 
    (
        %11111100,
        %10000100, 
        %10000100,
        %10000100,
        %10000100,
        %11111100,
        %00000000
    );
    char2: array [0..6] of byte = 
    (
        %11111100,
        %10000100, 
        %10110100,
        %10110100,
        %10000100,
        %11111100,
        %00000000
    );

{$i helpers.inc}
{$i fileio.inc}
{$i config.inc}
{$i gui.inc}

begin

    NeoSetChar(192,@char1);
    NeoSetChar(193,@char2);
    InitConfig;
    GuiInit();
    RefreshScreen(true);
    ShowTitle;
    while Keypressed do ReadKey; // clear keyboar buffer

    repeat
        NeoWaitForVblank;
        
        if reload then
            begin
                ShowPanel(current_panel);
                reload := false;
            end;
        
        GetUserInput;
        case key.code of 
            KEY_PAGEUP: 
                if panelCursor>0 then begin
                    panelCursor:=panelCursor-PANEL_HEIGHT;
                    if panelCursor<0 then panelCursor := 0;
                    reload := true;
                end;

            KEY_PAGEDOWN: 
                if panelCursor<dirCount-1 then begin
                    panelCursor:=panelCursor+PANEL_HEIGHT;
                    if panelCursor>dirCount-1 then panelCursor := dirCount-1;
                    reload := true;
                end;

            KEY_DOWN: KeyDown;
            KEY_UP: KeyUp;
            KEY_ENTER: EntryClicked(entrylist[panelCursor]);
            KEY_SPACE: SelectEntry(panelCursor);

            KEY_INSERT: begin 
                SelectEntry(panelCursor);
                KeyDown;
            end;
            KEY_BACKSPACE: if panel_depth[current_panel]>0 then begin   
                    reload := FolderUp;
                    if reload then begin
                        ShowTopBar;
                        FetchDir(current_panel);   
                    end;
                end;
            KEY_TAB: SwitchPanel;
            KEY_F1: ShowHelp;
            KEY_F2: PromptRename;
            KEY_F5: CopyDialog;
            KEY_F6: MoveDialog;
            KEY_F7: PromptNewDir;
            KEY_F8: PromptDelete;
            KEY_F9: ShowOptions;
            KEY_F10: NeoReset; 
            KEY_KPASTERISK: SelectionToggleAll(current_panel);
            KEY_GRAVE: ShowTitle;

            KEY_ESC: if CountSeleted(current_panel)>0 then begin
                ClearSelected(current_panel);
                reload := true;
            end;

        end;

        // check if window position is valid, and fix if not
        if PANEL_HEIGHT+panelTop>dirCount then begin
            panelTop:= dirCount-PANEL_HEIGHT;
        end;
        if panelTop<0 then panelTop:=0;
        // check if cursor position is valid, and fix if not
        if panelCursor < 0 then panelCursor := 0;
        if panelCursor >= dirCount then panelCursor := dirCount - 1;
        i := panelCursor - panelTop;
        if i>=PANEL_HEIGHT then panelTop := panelTop + i - PANEL_HEIGHT + 1;
        if i<0 then panelTop := panelTop + i;
    
        // store listtop and cursor position
        panel_curpos[current_panel,panel_depth[current_panel]] := panelCursor;
        panel_top[current_panel,panel_depth[current_panel]] := panelTop;

    until quit;


end.
