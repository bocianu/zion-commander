procedure MakeSpaceBar(w:byte);
begin
    SetLength(bar,w);
    FillChar(@bar[1],w,' ');
end;


function Min(a,b:integer):integer;
begin
    if a<b then result:=a else result:=b;
end;

function FormatSize(size:integer):string[15];
var nsize:integer;
    dividor:word;
    divcount:byte;
    sizepostfix:array[0..2]of char = ('K','M','G');
begin
    begin
        dividor:=1024;
        divcount:=1;
        NeoStr(size,result);
        SetMathStack(dividor,1);
        while (length(result)>5) and (divcount<3) do begin
            SetMathStack(size,0);
            for b:=1 to divcount do DoMathOnStack(MATHIDiv);
            nsize := GetMathStackInt(0);
            NeoStr(nsize, result);
            result := concat(result, sizepostfix[divcount-1]);                    
            inc(divcount);
        end;
    end;
    if (length(result)>5) or (result[1]='-') then result:='+2G';
end;


function PromptStr(var s:string):boolean;
var k:char;
begin
    while keypressed do readkey;
    Write(s);
    repeat 
        k:=ReadKey;
        case k of
            '0'..'9','a'..'z','A'..'Z','_','.': begin
                Inc(s[0]);
                Write(k);
                s[byte(s[0])]:=k;
            end;
            #8: if s[0]>#0 then begin
                Write(k,#5,' ',#1);
                Dec(s[0]);
            end;
        end;
    until (k=#13) or (k=#27);
    result := (k <> #27);
end;

procedure SetEntry(i:byte);
begin
    dirEntry := pointer(ENTRIES_ADDRESS + (i shl 5));
end;

function isFolder: boolean;
begin
    result := (dirEntry.attr and FI_ATTR_DIRECTORY) <> 0;
end;

function isParent: boolean;
begin
    result := (dirEntry.attr and 128) <> 0;
end;

procedure FullPathToTmp(cp:byte;var filename:string);
begin
    if panel_depth[cp]>0 then 
        tmp := concat(panel_cwd[cp],'/')
    else tmp := panel_cwd[cp];
    tmp := concat(tmp,filename);
end;

procedure ClearSelected(cp:byte);
var i:byte;
begin
    for i:=0 to MAX_DIR_ENTRIES-1 do selected[cp,i]:=0;
end;

procedure SelectionToggleAll(cp:byte);
var i:byte;
begin
    for i:=0 to dirCount-1 do begin
        SetEntry(entrylist[i]);
        if not isParent then selected[cp,i]:=selected[cp,i] xor 1;
    end;
    reload:=true;
end;

procedure SelectEntry(id:byte);
begin
    SetEntry(entrylist[i]);
    if not isParent then selected[current_panel,id] := selected[current_panel,id]  xor 1;
    reload := true;
end;

function CountSeleted(cp:byte):byte;
var i:byte;
begin
    result:=0;
    for i:=0 to dirCount-1 do if selected[cp,i]<>0 then Inc(result);
end;


