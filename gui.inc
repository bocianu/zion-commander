procedure GuiInit;
begin
    tmp := '/';

    panel_depth[PANEL_LEFT]:=0;
    panel_cwd[PANEL_LEFT]:=tmp;
    panel_curpos[PANEL_LEFT,0]:=0;
    panel_top[PANEL_LEFT,0]:=0;

    panel_depth[PANEL_RIGHT]:=0;
    panel_cwd[PANEL_RIGHT]:=tmp;
    panel_curpos[PANEL_RIGHT,0]:=0;
    panel_top[PANEL_RIGHT,0]:=0;

    ClearSelected(PANEL_LEFT);
    ClearSelected(PANEL_RIGHT);

    quit := false;
    reload := false;
    joyDelay := 0;
    panelCursor := 0;
    panelTop := 0;
    current_panel:=PANEL_LEFT;

end;

procedure ShowTopBar;
begin
    MakeSpaceBar(SCREEN_WIDTH);
    tmp := concat(panel_name[current_panel], ':');
    tmp := concat(tmp, panel_cwd[current_panel]);
    move(@tmp[1], @bar[1], Length(tmp));
    Gotoxy(1,1);
    TextBackground(4);
    TextColor(6);
    Write(bar);
end;

procedure ShowTitle;
begin
    Gotoxy(31,1);
    TextBackground(4);
    TextColor(6);
    Write('Zion Commander v.',VER);
end;


procedure ShowBottomBar;
begin
    bar := #$86' '#$84#$96'F2'#$86#$94' Rename '#$84#$96'F5'#$86#$94' Copy '#$84#$96'F6'#$86#$94' Move '#$84#$96'F7'#$86#$94' NewDir '#$84#$96'F8'#$86#$94' Del '#$84#$96'F10'#$86#$94' Exit';
    Gotoxy(1,PANEL_HEIGHT+2);
    Write(bar);
end;

procedure showStatusBar(cp:byte);
var cnt,sel:byte;
    
begin
    TextBackground($0D);
    MakeSpaceBar(PANEL_WIDTH);
    Gotoxy(panel_x[cp],PANEL_HEIGHT+1);
    Write(bar);
    cnt := dirCount;
    if panel_depth[cp]>0 then dec(cnt);
    sel:=CountSeleted(cp);
    if sel>0 then begin
        TextColor($0c);    
        NeoStr(sel,tmp);
        tmp := concat(tmp,' items');
        if sel=1 then dec(tmp[0]);
        tmp := concat(tmp,' selected');
    end else begin
        TextColor(8);
        NeoStr(cnt,tmp);
        tmp := concat(tmp,' items');
        if cnt=1 then dec(tmp[0]);
    end;
    Gotoxy(panel_x[cp],PANEL_HEIGHT+1);
    Write(tmp);
end;

procedure ShowPanel(cp:byte);
var i: word;
    c: string[15];
    lcount: byte;
begin
    lcount:=2;
    panelCursor := panel_curpos[cp,panel_depth[cp]];
    panelTop := panel_top[cp,panel_depth[cp]];  
    for i:=panelTop to panelTop+PANEL_HEIGHT-1 do
        begin
            MakeSpaceBar(PANEL_WIDTH);
            if (panelCursor = i) and (cp=current_panel) then TextBackground(7)
                else TextBackground(9);
            if i<dirCount then begin

                SetEntry(entrylist[i]);
                if isFolder then begin
                        TextColor(11);
                        c:='<dir>';
                    end
                else begin
                    TextColor(15);
                    c:=FormatSize(dirEntry.size);
                    //c:=FormatSize(dirEntry.attr);
                end;
                if dirEntry.filetype = FTYPE_EXECUTABLE then begin
                    TextColor(6);
                end;

                if selected[cp,i]=1 then TextColor(1);
                
                Move(@dirEntry.name[1],@bar[1 ],Min(PANEL_WIDTH-5,ord(dirEntry.name[0])));
                Move(c[1],bar[PANEL_WIDTH-length(c)+1],length(c));
    
            end;
            Gotoxy(panel_x[cp],lcount);
            Write(bar);
            Inc(lcount);
        end;
    if config.showPanelStatus=1 then showStatusBar(cp);
end;

function readJoy:byte;
begin
    result:=0;
    b := NeoGetJoy(1);
    if b and 1 <> 0 then result := KEY_LEFT; // left 
    if b and 2 <> 0 then result := KEY_RIGHT; // right
    if b and 4 <> 0 then result := KEY_UP; // up 
    if b and 8 <> 0 then result := KEY_DOWN; // down 
    if b and 16 <> 0 then result := KEY_SPACE; // butA
    if b and 32 <> 0 then result := KEY_ENTER; // butB
end;

procedure GetUserInput;
begin
    //while keypressed do ReadKey;
    
    ReadKeyboard(key);
    IF key.code = 0 then key.code := readJoy;

    if key.code = 0 then begin
        joyDelay := 0;
        joyCount := 0;
    end;
    
    if joyDelay = 0 then begin 
        if key.code <> 0 then begin 
            joyDelay := JOY_DELAY;
            if joyCount > 0 then begin
                joyDelay := JOY_REPEAT;
            end else Inc(joyCount);
        end;
    end else begin  
        Dec(joyDelay);
        key.code := 0;
    end;
end;

procedure WaitForKey;
begin
    repeat ReadKeyboard(key) until key.code=0;
    repeat GetUserInput until key.code<>0;    
end;

procedure RefreshScreen(clear:boolean);
var cp:byte;
begin
    NeoResetPalette;
    TextBackground(4);
    IF clear then ClrScr;
    NeoWaitForVblank;
    ShowBottomBar;
    cp:=current_panel xor 1;
    FetchDir(cp);
    ShowPanel(cp);
    FetchDir(current_panel);
    ShowPanel(current_panel);
    ShowTopBar;
end;

procedure SwitchPanel;
begin
    current_panel := current_panel xor 1;
    NeoChangeDirectory(@panel_cwd[current_panel]);
    RefreshScreen(false);
end;

procedure ModalNextLine(n:byte);
begin
    Inc(modalTop,n);
    Gotoxy(MODAL_LEFT+1+modalMarginL, modalTop+1);
end;

procedure ShowModal(h:byte);
begin
    TextBackground($0c);
    TextColor($e);
    MakeSpaceBar(MODAL_WIDTH);
    modalTop := MODAL_MID - (h shr 1) - 1;
    modalMarginL := 2;
    NeoSetDefaults(0,8,1,0,0);
    NeoDrawRect(MODAL_LEFT*6,modalTop*8-1,((MODAL_LEFT+MODAL_WIDTH)*6)+1,(modalTop+h)*8+2);
    NeoClearRegion(MODAL_LEFT,modalTop,MODAL_LEFT+MODAL_WIDTH-1,modalTop+h-1);
    ModalNextLine(1);
end;

procedure PromptNewDir;
begin
    ShowModal(5);
    Write('New directory name:');
    ModalNextLine(2);
    tmp:='';
    if PromptStr(tmp) then NeoCreateDirectory(@tmp);
    RefreshScreen(true);
end;

procedure PromptRename;
begin
    ShowModal(5);
    SetEntry(entrylist[panelCursor]);
    Write('New name:');
    ModalNextLine(2);
    tmp := dirEntry.name;
    if PromptStr(tmp) then NeoRenameFile(@dirEntry.name,@tmp);
    RefreshScreen(true);
end;

procedure ShowHelp;
begin
    ShowModal(24);
    bar := #$90#$8c'          Zion Commander Help            '#$9c#$8e;
    Gotoxy(MODAL_LEFT+1,modalTop);
    Write(bar);
    ModalNextLine(1);
    Write('Simple file manager for Neo6502');
    ModalNextLine(1);
    Write('written in Mad-Pascal by bocianu.');
    ModalNextLine(1);
    Write('version ',VER);
    ModalNextLine(2);
    Write('Press '#$83'K'#$8e' to see shortcuts.');
    ModalNextLine(2);
    Write('Press any other key to close.');
    WaitForKey;
    
    if key.code = KEY_K then begin
    ShowModal(24);
        bar := #$90#$8c'        Zion Commander Shortcuts         '#$9c#$8e;
        Gotoxy(MODAL_LEFT+1,modalTop);
        Write(bar);
        ModalNextLine(2);
        Write(#$83'Up'#$8e', '#$83'Down'#$8e', '#$83'PageUp'#$8e', '#$83'PageDown'#$8e': Navigate');
        ModalNextLine(2);
        Write(#$83'F1'#$8e': Help     '#$83'F2'#$8e': Rename   '#$8d'F3'#$8d': View');
        ModalNextLine(2);
        Write(#$8d'F4'#$8d': Edit     '#$83'F5'#$8e': Copy     '#$83'F6'#$8e': Move');
        ModalNextLine(2);
        Write(#$83'F7'#$8e': New Dir  '#$83'F8'#$8e': Delete   '#$83'F9'#$8e': Options');
        ModalNextLine(2);
        Write(#$83'F10'#$8e': Exit    '#$83'TAB'#$8e': Change Panel');
        ModalNextLine(2);
        Write(#$83'Space'#$8e', '#$83'Insert'#$8e': Select item  '#$83'*'#$8e': Invert');
        ModalNextLine(2);
        Write(#$83'BackSpace'#$8e': Back to parent folder');
        ModalNextLine(2);
        Write(#$83'Enter'#$8e': Launch entry or enter folder');
        ModalNextLine(5);
        Write('Press any key to close.');
        WaitForKey;
    end;
    RefreshScreen(true);
end;

procedure WriteCheckBox(b:boolean);
begin
    if b then Write(#193) else Write(#192);
end;

procedure ShowOptions;
var done:boolean;
begin
    done:=false;
    StoreConfig;
    repeat

        ShowModal(20);
        bar := #$90#$8c'         Zion Commander Options          '#$9c#$8e;
        Gotoxy(MODAL_LEFT+1,modalTop);
        Write(bar);
        ModalNextLine(1);
        WriteCheckBox(config.showHidden=1);
        Write(' Show '#$83'H'#$8e'idden files');
        ModalNextLine(2);
        WriteCheckBox(config.showPanelStatus=1);
        Write(' Show '#$83'P'#$8e'anel status');
        ModalNextLine(2);
        WriteCheckBox(config.execFirst=1);
        Write(' Show e'#$83'X'#$8e'ecutables first');
        ModalNextLine(2);
        WriteCheckBox(config.saveOnExit=1);
        Write(' Save on '#$83'E'#$8e'xit');
        ModalNextLine(10);
        Write(#$83'Enter'#$8e' to save       '#$83'Escape'#$8e' to Cancel');
        WaitForKey;
        case key.code of
            KEY_H: config.showHidden:=config.showHidden xor 1;
            KEY_P: config.showPanelStatus:=config.showPanelStatus xor 1;
            KEY_X: config.execFirst:=config.execFirst xor 1;
            KEY_E: config.saveOnExit:=config.saveOnExit xor 1;
            KEY_ENTER : begin    
                StoreConfig;
                done:=true;
            end;
            KEY_ESC : begin
                InitConfig;
                done:=true;
            end;
        end;
    until done;
    RefreshScreen(true);
end;

procedure WriteCenteredTmp;
begin
    modalMarginL := ((MODAL_WIDTH - Length(tmp)) shr 1) ;
    ModalNextLine(0);
    Write(tmp);
end;

function Confirm():boolean;
begin
    
    tmp := 'Yes / No';
    WriteCenteredTmp;
    WaitForKey;
    result := (key.code = KEY_Y);
end;

procedure PromptDelete;
var cnt:byte;
begin
    ShowModal(5);
    tmp:= 'Are you sure?';
    WriteCenteredTmp;
    ModalNextLine(2);
    if Confirm then begin
        cnt := CountSeleted(current_panel);
        if cnt = 0 then DeleteAtPos(panelCursor) 
        else begin
            for cnt:=0 to MAX_DIR_ENTRIES-1 do 
                if selected[current_panel,cnt]=1 then DeleteAtPos(cnt);
            ClearSelected(current_panel);
        end;
    end;
    RefreshScreen(true);
    reload:=true;
end;

procedure MoveDialog;
var cnt:byte;
begin
    ShowModal(3);
    tmp:='Moving files, please wait.';
    Write(tmp);
    cnt := CountSeleted(current_panel);
        if cnt = 0 then MoveEntry(panelCursor) 
        else begin
            for cnt:=0 to MAX_DIR_ENTRIES-1 do 
                if selected[current_panel,cnt]=1 then MoveEntry(cnt);
            ClearSelected(current_panel);
        end;
    RefreshScreen(true);
    reload:=true;
end;

procedure CopyDialog;
var cnt:byte;
begin
    ShowModal(3);
    tmp:='Copying files, please wait.';
    Write(tmp);
    cnt := CountSeleted(current_panel);
        if cnt = 0 then CopyEntry(panelCursor) 
        else begin
            for cnt:=0 to MAX_DIR_ENTRIES-1 do 
                if selected[current_panel,cnt]=1 then CopyEntry(cnt);
            ClearSelected(current_panel);
        end;
    RefreshScreen(true);
    reload:=true;
end;

procedure EntryClicked(i:byte);
begin
    while keypressed do readkey;
    SetEntry(i);
    if isParent then reload := FolderUp
    else if isFolder then reload := FolderEnter;                 
    if reload then begin
        ShowTopBar;
        FetchDir(current_panel);   
    end else begin
        if dirEntry.filetype = FTYPE_EXECUTABLE then LaunchEntry(i);
        if dirEntry.filetype = FTYPE_BASICSOURCE then LaunchEntry(i);
        if dirEntry.filetype = FTYPE_BASICTOKENIZED then LaunchEntry(i);
        // other associated types can be added here
        RefreshScreen(true);
    end;
end;

procedure KeyDown;
begin
    if panelCursor<dirCount-1 then
        begin
            inc(panelCursor);
            reload := true;
        end;
end;

procedure KeyUp;
begin
    if panelCursor>0 then
        begin
            dec(panelCursor);
            reload := true;
        end;
end;
