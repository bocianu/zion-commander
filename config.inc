procedure StoreConfig;
begin
    NeoSave(CONFIG_FILE,word(@config),SizeOf(TConfig));
end;

procedure InitConfig;
begin
    if not NeoLoad(CONFIG_FILE,word(@config)) then begin
        config.showHidden:=0;
        config.showPanelStatus:=1;
        config.execFirst:=1;
        config.saveOnExit:=1;
        StoreConfig;
    end;
end;