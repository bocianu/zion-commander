Zion Commander is a simple file management program for the Neo6502 inspired by the famous Norton Commander from the DOS era. It runs on the Neo6502 computer and allows you to manage file resources, run executable files. 

Some additional features are planned such as:
- loading basic files, 
- editing files, 
- viewing gfx resources and so on....

### Shortcuts:  
Cursor up/down - navigation through the active panel  
Page up/down - navigation through the active panel (faster)  
TAB - switch between panels  
F2 - change file name  
F5 - copy file(s)  
F6 - move file(s)  
F7 - create a new directory   
F8 - delete file(s)  
F10 - exit  
Enter - run current file (so far only works for executable files with .neo extension)  
Spacebar - select a file  
Insert - select a file and move the cursor down.  
\*  - select all / toggle selection  
Esc - clears current selection  
Backspace - move back to the parent directory  

### Knows issues:
- copying of folders does not work yet

